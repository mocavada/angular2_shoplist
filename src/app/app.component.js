/*
 * app.component.js
 * Manages the view and business-logic of the app.
 */
(function( app ){
    
    /**
     * Represents a single grocery item.
     * @constructor
     * @param {String} name - the name of the grocery item
     * @param {String} details - more detailed information 
     * about the grocery item
     */
    
    var GroceryItem = ng.core.Class({
        constructor : function( name, details ){
            this.name       = name;
            this.details    = details;
        }
    });
    
    /**
     * The main app component that handles
     * the view as well as the data needed.
     * @constructor
     */
    app.AppComponent = 
        ng.core.Component({
            selector : 'shopping-list',
            template : `<header>
                            <h1>{{title}}</h1>
                        </header>
                        <section>
                            <h3>Add a New Item</h3>
                            <input [(ngModel)]="newItemName"
                                   type="text"
                                   size="60" 
                                   placeholder="Name" />
                            <input [(ngModel)]="newItemDetails"
                                   type="text"
                                   size="60"
                                   placeholder="Details"/>
                            <button
                                (click)="add(newItemName,newItemDetails)">
                                Add
                            </button>
                        </section>
                        <section>
                            <h3>Items to Buy</h3>
                            <ul class="items-to-buy">
                                <li *ngFor="let groceryItem of items">
                                    <h4>{{groceryItem.name}}</h4>
                                    <p>{{groceryItem.details}}</p>
                                    <button
                                        (click)="checkItem(groceryItem)">
                                        Check!
                                    </button>
                                </li>
                            </ul>
                        </section>`
        }).Class({
            constructor : function(){
                this.title = 'Shopping List';
                this.retrieve();
            },
            /**
             * Adds a new grocery item to the items list
             * @param {String} itemName - the name of the new item to add
             * @param {String} itemDetails - the details of the new item to add
             */
            add : function( itemName, itemDetails ){
                var item = new GroceryItem( itemName, itemDetails );
                this.items.push( item );
                
                this.newItemName = '';
                this.newItemDetails = '';
                
                this.store();
            },
            /**
             * Removes an item from the grocery list
             * @param {GroceryItem} item - the item to remove from the list
             */
            checkItem: function( item ){
                var index = this.items.indexOf( item );
                this.items.splice( index, 1 );
                
                this.store();
            },
            /**
             * Serializes the grocery list and saves it to LocalStorage
             */
            store : function (){
                var stringItems = JSON.stringify( this.items );
                window.localStorage.setItem( 'items', stringItems );
            },
            /**
             * Loads the grocery list from LocalStorage an unserializes it
             */
            retrieve : function(){
                this.items 
                    = JSON.parse( window.localStorage.getItem( 'items' ) );
                
                if( !this.items ){
                    this.items = [];
                }
            }
        });
})( window.app || ( window.app = {} ) );