/*
 * app.module.js
 * Groups the components the app needs together, and loads them.
 */
(function( app ){
    
    app.AppModule = 
        ng.core.NgModule({
            imports : [ ng.platformBrowser.BrowserModule, 
                        ng.forms.FormsModule ],
            declarations: [ app.AppComponent ],
            bootstrap : [ app.AppComponent ]
        }).Class({
            constructor : function(){
                // this constructor will run when the module loads
            }
        });
    
})( window.app || ( window.app = {} ) );